=== WT Service Manager ===
Contributors: kustyrt
Tags: service, услуги, pricelist, price list, service manager, менеджер услуг
Requires at least: 4.4.2
Tested up to: 4.4.2
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Управление оказываемыми услугами на страницах сайта

== Description ==

WT Service Manager - плагин для CMS WordPress, позволяющий с легкостью управлять оказываемыми услугами и ценами.

= Основные возможности плагина: =
* Менеджер услуг: Добавление, редактирование, удаление;
* Вывод прайс-листа на страницах сайта с помощью шорткода;
* Вывод подходящих по тематике услуг в боковой панели (сайдбаре) с помощью виджета.

Разработка и поддержка: [АИТ "Web Technology"](http://web-technology.biz/).

Домашняя страница и документация: [WT Service Manager](http://web-technology.biz/cms-wordpress/plugin-wt-service-manager-for-cms-wordpress).

== Installation ==

Процесс инсталляции плагина стандартен для WordPress.

1. Найти плагин в панели администратора вашего сайта:
Плагины->Добавить новый->Поле ввода "Поиск плагинов"-> Ввести "WT Service Manager".
2. Установить найденный плагин.
3. Активировать плагин WT Service Manager:
Плагины->Установленные->Активировать WT Service Manager.

== Changelog ==

= 0.8 =
* Создание плагина
* Менеджер услуг
* Отображение услуг с помощью шорткода wt_service_table
* Виджет для отображения услуг
