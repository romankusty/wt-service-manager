<?php

class WtServiceWidget extends WP_Widget {
    var $template = '_wt_service_item.php';

    function __construct() {
        parent::__construct(
            'wt_service_widget',
            'Услуги', // заголовок виджета
            array( 'description' => 'Отображение услуг.' ) // описание
        );
    }

    /*
     * фронтэнд виджета
     */
    public function widget( $args, $instance ) {
        $service_category = apply_filters( 'widget_title', $instance['service_category'] ); // к заголовку применяем фильтр (необязательно)
        $service_amount = $instance['service_amount'];

        $theme_root = get_theme_root().'/'.get_template();

        if (file_exists($theme_root.'/_wt_service_item.php'))
            $this->template = $theme_root.'/_wt_service_item.php';

        $services = get_posts(
            array(
                'service_category' => $service_category,
                'post_type' => 'service',
                'numberposts' => $service_amount,
                'orderby' => 'menu_order',
                'order' => 'ASC',
            )
        );

        foreach ($services as $service) {
            $service_title = $service->post_title;
            $service_content = $service->post_content;
            $service_price = get_post_meta($service->ID, 'price', true);

            echo $args['before_widget'];
            include $this->template;
            echo $args['after_widget'];
        }

    }

    /*
     * бэкэнд виджета
     */
    public function form( $instance ) {
        if (isset( $instance['service_category'])) {
            $service_category = $instance[ 'service_category' ];
        }
        if (isset($instance[ 'service_amount' ] ) ) {
            $service_amount = $instance[ 'service_amount' ];
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'service_category' ); ?>">Категории</label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'service_category' ); ?>" name="<?php echo $this->get_field_name( 'service_category' ); ?>" type="text" value="<?php echo esc_attr( $service_category ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'service_amount' ); ?>">Количество услуг:</label>
            <input id="<?php echo $this->get_field_id( 'service_amount' ); ?>" name="<?php echo $this->get_field_name( 'service_amount' ); ?>" type="text" value="<?php echo ($service_amount) ? esc_attr( $service_amount ) : '5'; ?>" size="3" />
        </p>
        <?php
    }

    /*
     * сохранение настроек виджета
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['service_category'] = ( ! empty( $new_instance['service_category'] ) ) ? strip_tags( $new_instance['service_category'] ) : '';
        $instance['service_amount'] = ( is_numeric( $new_instance['service_amount'] ) ) ? $new_instance['service_amount'] : '5'; // по умолчанию выводятся 5 постов
        return $instance;
    }
}